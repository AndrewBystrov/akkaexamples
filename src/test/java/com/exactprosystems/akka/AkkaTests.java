package com.exactprosystems.akka;

import akka.actor.*;
import akka.dispatch.OnComplete;
import akka.testkit.javadsl.TestKit;
import com.exactprosystems.akka.messages.ConvertToUpper;
import com.exactprosystems.akka.messages.EndTransaction;
import com.exactprosystems.akka.messages.Sleep;
import com.exactprosystems.akka.messages.StartTransaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AkkaTests {

	private ActorSystem system;

	@Before
	public void init() {
		this.system = ActorSystem.create("testActorSystem");
		this.system.log().info("Akka system created");
		this.system.whenTerminated().onComplete(new OnComplete<Terminated>() {
			@Override
			public void onComplete(Throwable failure, Terminated success) throws Throwable {
				if (failure == null) {
					System.out.println("Stopped successful");
				} else {
					failure.printStackTrace();
				}
			}
		}, this.system.dispatcher());
	}

	@Test
	public void testExpectClass() {
		this.shell((testKit, actor) -> {
			String in = "hello world!";
			actor.tell(new ConvertToUpper(in, null), testKit.getRef());

			String out = testKit.expectMsgClass(Duration.ofSeconds(1), String.class);
			assertEquals(in.toUpperCase(), out);
		});
	}

	@Test
	public void testExpectMsg() {
		this.shell((testKit, actor) -> {
			String in = "hello world!";
			actor.tell(new ConvertToUpper(in, null), testKit.getRef());

			String out = testKit.expectMsg(in.toUpperCase());
			assertEquals(in.toUpperCase(), out);
		});
	}

	@Test
	public void testWithSleep() {
		this.shell((testKit, actor) -> {
			String in = "foo bar";
			actor.tell(new Sleep(4000), testKit.getRef());
			actor.tell(new ConvertToUpper(in, null), testKit.getRef());

			testKit.expectNoMessage(Duration.ofSeconds(2));
			testKit.awaitCond(testKit::msgAvailable);

			String out = testKit.expectMsgClass(String.class);
			assertEquals(in.toUpperCase(), out);
		});
	}

	@Test
	public void testTransaction() {
		this.shell((testKit, actor) -> {
			String transactionId = "TransactionId";
			actor.tell(new StartTransaction(transactionId), testKit.getRef());

			List<String> inList = Arrays.asList("java", "Saratov", "meetup");
			for (String in : inList) {
				actor.tell(new ConvertToUpper(in, transactionId), testKit.getRef());
				testKit.expectNoMessage(Duration.ofSeconds(1));
			}

			actor.tell(new EndTransaction(transactionId), testKit.getRef());
			Set<String> expected = inList.stream()
					.map(String::toUpperCase)
					.collect(Collectors.toSet());

			testKit.receiveN(3)
					.stream()
					.map(obj -> (String) obj)
					.forEach(out -> assertTrue(expected.remove(out)));

			assertTrue(expected.isEmpty());
		});
	}

	@Test
	public void testWithUnhandled() {
		this.shell((testKit, actor) -> {
			boolean subscribe = this.system.getEventStream().subscribe(testKit.getRef(), UnhandledMessage.class);
			assertTrue(subscribe);

			actor.tell(new BigInteger("123"), testKit.getRef());

			testKit.awaitCond(testKit::msgAvailable);
			UnhandledMessage message = testKit.expectMsgClass(UnhandledMessage.class);
			Object out = message.getMessage();
			assertTrue(out instanceof BigInteger);
			assertEquals(123, ((BigInteger) out).intValue());
		});
	}

	@After
	public void clean() {
		if (this.system != null) {
			TestKit.shutdownActorSystem(this.system);
		}
	}

	private void shell(BiConsumer<TestKit, ActorRef> testKit) {
		new TestKit(this.system) {{
			Props props = Props.create(TestActor.class);
			ActorRef testActor = system.actorOf(props, "testActor");

			testKit.accept(this, testActor);

			this.within(Duration.of(3, ChronoUnit.SECONDS), () -> {
				this.expectNoMessage();
				return null;
			});
		}};
	}
}
