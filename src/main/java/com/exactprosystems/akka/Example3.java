package com.exactprosystems.akka;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Kill;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.dispatch.OnComplete;
import akka.pattern.Patterns;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.exactprosystems.akka.Common.createAkkaSystem;
import static com.exactprosystems.akka.Common.sleep;

public class Example3 {
	public static void main(String[] args) {
		ActorSystem system = createAkkaSystem();
		{
			ActorRef myActor = system.actorOf(Props.create(MyActor.class), "MyActorName1");
			myActor.tell(new StopMessage(), ActorRef.noSender());
		}
		sleep(2000);

		{
			ActorRef myActor = system.actorOf(Props.create(MyActor.class), "MyActorName2");
			myActor.tell(PoisonPill.getInstance(), ActorRef.noSender());
		}
		sleep(2000);

		{
			ActorRef myActor = system.actorOf(Props.create(MyActor.class), "MyActorName3");
			myActor.tell(Kill.getInstance(), ActorRef.noSender());
		}
		sleep(2000);

		{
			ActorRef myActor1 = system.actorOf(Props.create(MyActor.class), "StoppedActorName1");
			ActorRef myActor2 = system.actorOf(Props.create(MyActor.class), "StoppedActorName2");
			ActorRef myActor3 = system.actorOf(Props.create(MyActor.class), "StoppedActorName3");

			Function<ActorRef, Future<Boolean>> stopFunction = actor -> Patterns.gracefulStop(actor, Duration.create(5, TimeUnit.SECONDS));

			stopFunction.apply(myActor3)
					.flatMap(out -> stopFunction.apply(myActor1), system.dispatcher())
					.flatMap(out -> stopFunction.apply(myActor2), system.dispatcher())
					.flatMap(out -> {
						system.log().info("Actors were stopped");
						return system.terminate();
					}, system.dispatcher())
					.onComplete(new OnComplete<Terminated>() {
						@Override
						public void onComplete(Throwable failure, Terminated success) throws Throwable {
							if (failure != null) {
								failure.printStackTrace();
							} else {
								System.out.println(success);
							}
						}
					}, system.dispatcher());
		}
	}

	private static class MyActor extends AbstractLoggingActor {
		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.match(StopMessage.class, message -> getContext().stop(self()))
					.matchAny(message -> log().warning("Unhandled msg {} from {}", message, getSender()))
					.build();
		}

		@Override
		public void postStop() throws Exception {
			log().info("Actor [{}] stopped", self().path().name());
			super.postStop();
		}
	}

	private static class StopMessage {
		public StopMessage() {
		}

		@Override
		public String toString() {
			return "Stop []";
		}
	}
}
