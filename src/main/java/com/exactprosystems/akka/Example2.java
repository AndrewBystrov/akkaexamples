package com.exactprosystems.akka;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import static com.exactprosystems.akka.Common.createAkkaSystem;
import static com.exactprosystems.akka.Common.sleep;
import static com.exactprosystems.akka.Common.stopAkkaSystem;

public class Example2 {
	public static void main(String[] args) {
		ActorSystem system = createAkkaSystem();

		ActorRef myActor = system.actorOf(Props.create(MyActor.class), "MyActorName");
		myActor.tell(new InitMessage(), ActorRef.noSender());

		sleep(1000);
		myActor.tell(new StopMessage(), ActorRef.noSender());

		sleep(2000);
		stopAkkaSystem(system);
	}

	private static class MyActor extends AbstractLoggingActor {
		@Override
		public void aroundReceive(PartialFunction<Object, BoxedUnit> receive, Object msg) {
			log().info("Actor {} received msg [{}] from {}", MyActor.class.getSimpleName(), msg, sender());
			super.aroundReceive(receive, msg);
		}

		@Override
		public void preStart() throws Exception {
			log().info("Pre start method");
			super.preStart();
		}

		@Override
		public void postStop() throws Exception {
			log().info("Post stop method");
			super.postStop();
		}

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.match(InitMessage.class, message -> log().info("Received init message : {}", message))
					.match(StopMessage.class, message -> getContext().stop(getSelf()))
					.matchAny(message -> log().warning("Unhandled msg {} from {}", message, getSender()))
					.build();
		}
	}

	private static class InitMessage {
		public InitMessage() {
		}

		@Override
		public String toString() {
			return "InitMessage []";
		}
	}

	private static class StopMessage {
		public StopMessage() {
		}

		@Override
		public String toString() {
			return "StopMessage []";
		}
	}
}
