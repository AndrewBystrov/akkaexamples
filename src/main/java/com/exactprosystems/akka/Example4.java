package com.exactprosystems.akka;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorPath;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.AllForOneStrategy;
import akka.actor.OneForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.japi.pf.DeciderBuilder;
import scala.PartialFunction;
import scala.concurrent.duration.Duration;

import java.lang.reflect.Constructor;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static akka.actor.SupervisorStrategy.escalate;
import static akka.actor.SupervisorStrategy.restart;
import static akka.actor.SupervisorStrategy.resume;
import static akka.actor.SupervisorStrategy.stop;
import static com.exactprosystems.akka.Common.sleep;
import static com.exactprosystems.akka.Common.stopAkkaSystem;

public class Example4 {
	public static void main(String[] args) {
		ActorSystem system = Common.createAkkaSystem();

		final ActorRef myActor = system.actorOf(Props.create(MyActor.class), "MyActor");

		long ms = 3000;

		myActor.tell(Props.create(Child.class, ArithmeticException.class), ActorRef.noSender());
		sleep(ms);
		system.log().info("####");

		myActor.tell(Props.create(Child.class, NullPointerException.class), ActorRef.noSender());
		sleep(ms);
		system.log().info("####");

		myActor.tell(Props.create(Child.class, IllegalArgumentException.class), ActorRef.noSender());
		sleep(ms);
		system.log().info("####");

		myActor.tell(Props.create(Child.class, Exception.class), ActorRef.noSender());
		sleep(ms);

		stopAkkaSystem(system);
	}

	private static class MyActor extends AbstractLoggingActor {

		private static PartialFunction<Throwable, SupervisorStrategy.Directive> decider = DeciderBuilder
				.match(ArithmeticException.class, e -> resume())
				.match(NullPointerException.class, e -> restart())
				.match(IllegalArgumentException.class, e -> stop())
				.matchAny(e -> escalate())
				.build();

		private static SupervisorStrategy oneForOne = new OneForOneStrategy(10, Duration.create(1, TimeUnit.SECONDS), decider);
		private static SupervisorStrategy allForOne = new AllForOneStrategy(10, Duration.create(1, TimeUnit.SECONDS), decider);

		@Override
		public SupervisorStrategy supervisorStrategy() {
			return oneForOne;
//			return allForOne;
		}

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.match(Props.class, props -> {
						ActorRef child = getContext().actorOf(props);

						outChildren();

						child.tell("Throw exception", getSelf());
					})
					.matchAny(message -> log().warning("Unhandled msg {} from {}", message, getSender()))
					.build();
		}

		private void outChildren() {
			log().info("Children of MyActor : {}", StreamSupport.stream(this.getContext().getChildren().spliterator(), false)
					.map(ActorRef::path)
					.map(ActorPath::name)
					.collect(Collectors.joining(", ", "[", "]")));
		}

		@Override
		public void preStart() throws Exception {
			log().info("Started {}", self().path().name());
			super.preStart();
		}

		@Override
		public void postStop() throws Exception {
			log().info("Stopped {}", self().path().name());
			super.postStop();
		}
	}

	private static class Child extends AbstractLoggingActor {

		private Class<? extends Exception> exception;

		public Child(Class<? extends Exception> exception) {
			this.exception = exception;
		}

		@Override
		public void preStart() throws Exception {
			log().info("preStart called {}", this);
			super.preStart();
		}

		@Override
		public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
			log().info("preRestart called {}", this);
			super.preRestart(reason, message);
		}

		@Override
		public void postStop() throws Exception {
			log().info("postStop called {}", this);
			super.postStop();
		}

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.match(String.class, message -> {
						log().info("Received : {}", message);
						Constructor<? extends Exception> constructor = this.exception.getConstructor(String.class);
						throw constructor.newInstance(message + " " + this.exception.getSimpleName());
					})
					.matchAny(message -> log().warning("Unhandled msg {} from {}", message, getSender()))
					.build();
		}

		@Override
		public String toString() {
			return "ChildActor [" + self().path().name() + "] " + this.exception.getSimpleName();
		}
	}
}
