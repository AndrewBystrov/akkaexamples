package com.exactprosystems.akka;

import akka.actor.AbstractLoggingActor;
import com.exactprosystems.akka.messages.ConvertToUpper;
import com.exactprosystems.akka.messages.EndTransaction;
import com.exactprosystems.akka.messages.Sleep;
import com.exactprosystems.akka.messages.StartTransaction;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestActor extends AbstractLoggingActor {

	private final Map<String, List<String>> transactionMap;

	public TestActor() {
		this.transactionMap = new HashMap<>();
	}

	@Override
	public void aroundReceive(PartialFunction<Object, BoxedUnit> receive, Object msg) {
		log().info("Received msg {} from {}", msg, sender());
		super.aroundReceive(receive, msg);
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(Sleep.class, message -> {
					Common.sleep(message.getMs());
				})
				.match(StartTransaction.class, message -> {
					this.transactionMap.put(message.getTransactionId(), new ArrayList<>());
				})
				.match(EndTransaction.class, message -> {
					this.transactionMap.remove(message.getTransactionId())
							.forEach(str -> sender().tell(str, getSelf()));
				})
				.match(ConvertToUpper.class, msg -> msg.getTransactionId() == null, message -> {
					sender().tell(message.getStr().toUpperCase(), self());
				})
				.match(ConvertToUpper.class, message -> {
					this.transactionMap.get(message.getTransactionId())
							.add(message.getStr().toUpperCase());
				})
				.matchAny(this::unhandled)
				.build();
	}
}
