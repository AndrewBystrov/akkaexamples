package com.exactprosystems.akka;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import static com.exactprosystems.akka.Common.*;

public class Example1 {

	public static void main(String[] args) {
		ActorSystem system = createAkkaSystem();

		ActorRef myActor = system.actorOf(Props.create(MyActor.class), "MyActorName");
		myActor.tell("Hello", ActorRef.noSender());

		sleep(2000);
		stopAkkaSystem(system);
	}

	private static class MyActor extends AbstractLoggingActor {

		@Override
		public void aroundReceive(PartialFunction<Object, BoxedUnit> receive, Object msg) {
			log().info("{} received msg {} from {}", getSelf(), msg, getSender());
			super.aroundReceive(receive, msg);
		}

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.match(String.class, message -> sender().tell(message.toUpperCase(), self()))
					.matchAny(message -> log().warning("Unhandled msg {} from {}", message, getSender()))
					.build();
		}
	}
}
