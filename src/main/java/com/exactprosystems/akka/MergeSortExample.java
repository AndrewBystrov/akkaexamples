package com.exactprosystems.akka;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.pattern.Patterns;
import scala.compat.java8.FutureConverters;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class MergeSortExample {

	public static void main(String[] args) throws Exception {
		ActorSystem system = Common.createAkkaSystem();

		int[] array = new Random().ints(10_000, 1, 1_000_000).toArray();
		system.log().info("Input : " + Arrays.toString(array));

		ActorRef actorRef = system.actorOf(Props.create(SortActor.class));

		FutureConverters.toJava(Patterns.ask(actorRef, new Input(array), 10_000))
				.toCompletableFuture()
				.thenApply(obj -> (Output) obj)
				.thenAccept(output -> {
					int[] a = output.array;
					system.log().info("Output : {}", Arrays.toString(a));
					system.log().info("check : {}", check(a));
				})
				.thenAccept(obj -> Common.stopAkkaSystem(system))
				.get();
	}

	private static class SortActor extends AbstractLoggingActor  {
		private ActorRef sender;

		@Override
		public Receive createReceive() {
			return receiveBuilder()
//					.match(Input.class, in -> in.array.length < 2, in -> {
//						sender().tell(new Output(in.array), self());
//						self().tell(PoisonPill.getInstance(),self());
//					})
					.match(Input.class, in -> in.array.length <= 4, in -> {
						int[] sorted = bubbleSort(in.array);
						sender().tell(new Output(sorted), self());

						self().tell(PoisonPill.getInstance(),self());
					})
					.match(Input.class, in -> {
						this.sender = sender();
						int[] a = in.array;
						int mid = a.length / 2;

						int[] left = new int[mid];
						System.arraycopy(in.array, 0, left, 0, mid);

						int[] right = new int[a.length - mid];
						System.arraycopy(in.array, mid, right, 0, right.length);

						ActorRef leftRef = getContext().actorOf(Props.create(SortActor.class));
						ActorRef rightRef = getContext().actorOf(Props.create(SortActor.class));

						leftRef.tell(new Input(left), self());
						rightRef.tell(new Input(right), self());
					})
					.match(Output.class, first -> getContext().become(
							receiveBuilder()
									.match(Output.class, second -> {
										int[] merge = merge(first.array, second.array);
										sender.tell(new Output(merge),self());

										self().tell(PoisonPill.getInstance(),self());
									})
									.build()
					))
					.matchAny(this::unhandled)
					.build();
		}
	}

	private static int[] bubbleSort(int[] a) {
		int length = a.length;
		int[] out = new int[length];
		System.arraycopy(a, 0, out, 0, length);

		for (int i = 0; i < length - 1; i++) {
			boolean swapped = false;
			for (int j = 0; j < length - i - 1; j++) {
				if (out[j] > out[j + 1]) {
					int temp = out[j];
					out[j] = out[j + 1];
					out[j + 1] = temp;
					swapped = true;
				}
			}

			if (!swapped) {
				break;
			}
		}
		return out;
	}

	private static int[] merge(int[] a, int[] b) {
		int[] c = new int[a.length + b.length];
		int i = 0;
		int j = 0;
		int k = 0;
		while (i < a.length && j < b.length) {
			if (a[i] <= b[j]) {
				c[k++] = a[i++];
			} else {
				c[k++] = b[j++];
			}
		}
		while (i < a.length) {
			c[k++] = a[i++];
		}
		while (j < b.length) {
			c[k++] = b[j++];
		}
		return c;
	}

	private static boolean check(int[] a) {
		return IntStream.range(1, a.length)
				.noneMatch(i -> a[i - 1] > a[i]);
	}

	private static class Input {
		int[] array;

		public Input(int[] array) {
			this.array = array;
		}

		@Override
		public String toString() {
			return String.format("In  : %s", Arrays.toString(array));
		}
	}

	private static class Output {
		int[] array;

		public Output(int[] array) {
			this.array = array;
		}

		@Override
		public String toString() {
			return String.format("Out : %s", Arrays.toString(array));
		}
	}
}
