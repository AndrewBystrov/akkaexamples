package com.exactprosystems.akka;

import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.dispatch.OnComplete;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Common {
	private static final Logger logger = LoggerFactory.getLogger(Common.class);
	private Common() {}

	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception ignored) {}
	}


	public static ActorSystem createAkkaSystem() {
		Config akkaConfig = ConfigFactory.parseResources("akka.conf");
		ActorSystem system = ActorSystem.create("actorSystem", akkaConfig);

		system.log().info("Actor system initialized");
		return system;
	}

	public static void stopAkkaSystem(ActorSystem system) {
		system.terminate()
				.onComplete(new OnComplete<Terminated>() {
					@Override
					public void onComplete(Throwable failure, Terminated success) throws Throwable {
						if (failure != null) {
							failure.printStackTrace();
						} else {
							logger.info(success.toString());
						}
					}
				}, system.getDispatcher());
	}
}
