package com.exactprosystems.akka.messages;

import javax.annotation.Nullable;
import java.util.StringJoiner;

public class ConvertToUpper {
	private final String str;
	@Nullable
	private final String transactionId;

	public ConvertToUpper(String str, @Nullable String transactionId) {
		this.str = str;
		this.transactionId = transactionId;
	}

	public String getStr() {
		return this.str;
	}

	@Nullable
	public String getTransactionId() {
		return this.transactionId;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", ConvertToUpper.class.getSimpleName() + "[", "]")
				.add("str='" + this.str + "'")
				.add("transactionId='" + this.transactionId + "'")
				.toString();
	}
}
