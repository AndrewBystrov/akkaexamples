package com.exactprosystems.akka.messages;

public class Sleep {
	private final long ms;

	public Sleep(long ms) {
		this.ms = ms;
	}

	public long getMs() {
		return this.ms;
	}

	@Override
	public String toString() {
		return "Sleep " + this.ms + " ms";
	}
}
