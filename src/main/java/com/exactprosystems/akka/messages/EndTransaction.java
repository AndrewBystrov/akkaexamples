package com.exactprosystems.akka.messages;

import java.util.StringJoiner;

public class EndTransaction {
	private final String transactionId;

	public EndTransaction(String id) {
		transactionId = id;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", EndTransaction.class.getSimpleName() + "[", "]")
				.add("transactionId='" + transactionId + "'")
				.toString();
	}
}
