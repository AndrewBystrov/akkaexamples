package com.exactprosystems.akka.messages;

import java.util.StringJoiner;

public class StartTransaction {
	private final String transactionId;

	public StartTransaction(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", StartTransaction.class.getSimpleName() + "[", "]")
				.add("transactionId='" + transactionId + "'")
				.toString();
	}
}
